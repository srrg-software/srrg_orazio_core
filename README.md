# DEPRECATED, USE SRRG2_ORAZIO

# srrg_orazio_core
The core package with firmware and installation instructions for
the Orazio Robot (aka MARRtino)

## Detailed Description

Orazio is a simple and cheap differential drive robot based on Arduino.
It performs closed loop control of the wheels, odometry integration
at the mobile base level.

The PC can control the robot through a serial connection.
All parameters can be configured and are stored in the EEPROM of the
arduino.

This package contains

* Arduino firmware
* Calibration Utilities

## Prerequisites

### Software

For this package you need
* Ubuntu 16.04 or Ubuntu 14.04
* arduino-mk 
* libncurses5-dev
* libwebsockets-dev
ROS is not required

### HARDWARE

* Arduino MEga 2560
* Arduino motor shield OR any 5V controllable H Bridge
* two motors with encoders
* a 12V battery
* a frame to fix all these devices
* a laptop to connect to Arduino board
* wheels and other mechanical parts

For a detailed part list look at 
https://sites.google.com/dis.uniroma1.it/marrtino/part-list?authuser=0

## Quick setup guide

### udev rules

Setting udev rule for Arduino board:

Create a file (e.g., /etc/udev/rules.d/90-arduino.rules) with the following lines
```
#Arduino MEGA
SUBSYSTEMS=="usb", ATTRS{idProduct}=="0042", ATTRS{idVendor}=="2341", SYMLINK+="orazio arduino_mega_$attr{serial}", MODE="0666"
SUBSYSTEMS=="usb", ATTRS{idProduct}=="0242", ATTRS{idVendor}=="2341", SYMLINK+="orazio arduino_mega_$attr{serial}", MODE="0666"
SUBSYSTEMS=="usb", ATTRS{idProduct}=="0010", ATTRS{idVendor}=="2341", SYMLINK+="orazio arduino_mega_$attr{serial}", MODE="0666"
```

Note: for Arduino clones check the idProduct with 'dmesg' command.

Restart udev service

```
sudo service udev restart
```


### Check serial port connection

* Connect the arduino

* Type the command "dmesg" and check lines like these

```
[  310.758803] usb 2-1: new full-speed USB device number 6 using xhci_hcd
[  310.889323] usb 2-1: New USB device found, idVendor=2341, idProduct=0010
[  310.889332] usb 2-1: New USB device strings: Mfr=1, Product=2, SerialNumber=220
[  310.889337] usb 2-1: Product: Arduino Mega 2560
[  310.889341] usb 2-1: Manufacturer: Arduino (www.arduino.cc)
[  310.889344] usb 2-1: SerialNumber: 75435363138351301152
[  310.889747] usb 2-1: ep 0x82 - rounding interval to 1024 microframes, ep desc says 2040 microframes
[  311.470876] cdc_acm 2-1:1.0: ttyACM0: USB ACM device
[  311.471107] usbcore: registered new interface driver cdc_acm
[  311.471110] cdc_acm: USB Abstract Control Model driver for USB modems and ISDN adapters
```

* Check that idVendor and idProduct matches the values entered in the udev rule.
If not, add a new rule with the values your read from 'dmesg'.

* Check the name of the port (ttyACM0 in the example above)

* Check the port is writeable (replace ttyACM0 with your actual port if different)

``` 
ls -l /dev/ttyACM0
crw-rw-rw- 1 root dialout 166, 0 <...> /dev/ttyACM0
```

You must see the three groups rw-rw-rw. If not, go back to the udev settings above.


* Check the symbolic link is correct

``` 
ls -l /dev/orazio 
lrwxrwxrwx 1 root root 7 mag 13 14:27 /dev/orazio -> ttyACM0
```
You shoud see /dev/orazio linked to the right ttyACM port

If not, go back to the udev settings above.




### Flashing Arduino

The first step is compiling and flasking the arduino with the firmware
To this end:
* connect the arduino
* make and upload the software

```
cd arduino
make upload
```

You should see something like this on your shell

```
avrdude: XXXXX bytes of flash written
avrdude: safemode: Fuses OK (E:FD, H:D8, L:FF)
avrdude done.  Thank you.
```

### Building the Monitor Program
Here we build a set of programs needed to contol the arduino running the firmware from the PC. You have to options

* Preferred build mode

Link srrg_orazio_core in a catkin workspace and compile with 'catkin_make'

Run the monitor
```
rosrun srrg_orazio_core orazio_robot_websocket_server -serial-device /dev/orazio -resource-path  `rospack find srrg_orazio_core`/html -rate 10

```

Options
* -serial-device /dev/orazio   should be the serial port where the arduino is detected
* -resource-path <html_folder> should be followed by the html directory in the package
* -rate  <int>                 web server refresh rate [Hz]

This will start the web server interface to control the robot.

Connect to the web server from a browser on port 9000 (e.g., http://localhost:9000/).

Proceed on that and execute the instructions to assemble and tune the robot.
Do that incrementally.

For remote wireless connection, use a low value of the rate (e.g., 1 Hz)
and connect to http://IP_of_PC_connected_with_arduino:9000/

Enjoy,
	G.

